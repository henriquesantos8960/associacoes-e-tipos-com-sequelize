import {ProductInput, ProductOutput} from "../database/models/ProductsModel";
import * as repository from "../database/repositories/ProductsRepository";

export const getAll = async (): Promise<ProductOutput[]> => {
    return await repository.getAll();
};

export const getById = async (id: number): Promise<ProductOutput> => {
    return await repository.getById(id);
};

export const create = async (payload: ProductInput): Promise<ProductOutput> => {
    return await repository.create(payload);
};

export const updateById = async (id: number, payload: ProductInput): Promise<ProductOutput> => {
    return await repository.updateById(id, payload);
};

export const deleteById = async (id: number): Promise<void> => {
    await repository.deleteById(id);
};