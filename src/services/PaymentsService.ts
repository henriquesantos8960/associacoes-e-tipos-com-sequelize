import {PaymentInput, PaymentOutput} from "../database/models/PaymentsModel";
import * as repository from "../database/repositories/PaymentsRepository";

export const getAll = async (): Promise<PaymentOutput[]> => {
    return await repository.getAll();
};

export const getById = async (id: number): Promise<PaymentOutput> => {
    return await repository.getById(id);
};

export const create = async (payload: PaymentInput): Promise<PaymentOutput> => {
    return await repository.create(payload);
};

export const updateById = async (id: number, payload: PaymentInput): Promise<PaymentOutput> => {
    return await repository.updateById(id, payload);
};

export const deleteById = async (id: number): Promise<void> => {
    await repository.deleteById(id);
};