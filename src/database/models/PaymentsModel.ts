import {DataTypes, Model, Optional} from "sequelize";
import {sequelize} from "../../database/sequelize";

interface PaymentAttribute {
    customerNumber: number,
    checkNumber: string,
    paymentDate: number,
    amount: number,
};

// Essas interfaces auxiliares são necessárias caso o id da tabela seja de auto incremento
export interface PaymentInput extends Optional<PaymentAttribute, "customerNumber">{};
export interface PaymentOutput extends Required<PaymentAttribute>{};

class Payment extends Model <PaymentAttribute, PaymentInput> {
    declare customerNumber: number;
    declare checkNumber: string;
    declare paymentDate: number;
    declare amount: number;
};

Payment.init({
    customerNumber: {type: DataTypes.NUMBER, primaryKey: true, allowNull: false},
    checkNumber: {type: DataTypes.STRING(50), allowNull: false},
    paymentDate: {type: DataTypes.DATEONLY, allowNull: false},
    amount: {type: DataTypes.DECIMAL(10, 2), allowNull: false},
}, {
    sequelize,
    modelName: "payments"
});

export default Payment;