import AppError from "../../utils/AppError";
import model, {PaymentInput, PaymentOutput} from "../models/PaymentsModel";

export const getAll = async (): Promise<PaymentOutput[]> => {
    return await model.findAll();
};

export const getById = async (id: number): Promise<PaymentOutput> => {
    const payment = await model.findByPk(id);

    if (!payment) {
        throw new AppError("NotFoundError", "Registro não encotrado", 404);
    };

    return payment;
};

export const create = async (payload: PaymentInput): Promise<PaymentOutput> => {
    return await model.create(payload);
};

export const updateById = async (id: number, payload: PaymentInput): Promise<PaymentOutput> => {
    const payment = await model.findByPk(id);

    if (!payment) {
        throw new Error("Registro não encontrado");
    };

    return await payment.update(payload);
};

export const deleteById = async (id: number): Promise<void> => {
    const payment = await model.findByPk(id);

    if (!payment) {
        throw new Error("Registro não encontrado");
    };

    await payment.destroy();
};