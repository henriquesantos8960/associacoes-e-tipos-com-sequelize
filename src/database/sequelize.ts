import {Error, Sequelize} from "sequelize";
import {initdb} from "./initdb";

export const sequelize = new Sequelize("classicmodels", "root", "", {
    host: "localhost",
    dialect: "mysql",
    define: {
        freezeTableName: true,
        timestamps: false
    },
    logging: false
});

export default () => {
    sequelize.authenticate()
    .then(() => {
        console.log("Conexão com o MySQL realizada com sucesso");
    })
    .catch((error: Error) => {
        console.log(`Conexão com o MySQL não foi bem sucedida: ${error}`);
    });
};